---
title: "Photos"
draft: false
ShowBreadCrumbs: false
hidemeta: true
tocopen: false
description: "These are some significant photos I have taken over the years.
All of these are densely charged with meaning and emotion to me.
But beauty is in the eye of the beholder:
I hope that they can tell a little story also to you,
even if different from the one they evoke in me."
---

### Back to reality
![Back to reality](1-BackToReality.jpg#center)
This photo was taken in Monza (Italy), in September 2018.

### A new life
![A new life](2-ANewLife.jpg#center)
This photo was shot by Giacomo Masiero in Parc de Peixotto, Talence (France), in October 2020.

### Dicono che c'è un tempo per aspettare
![Dicono che c'è un tempo per aspettare](3-DiconoCheC'èUnTempoPerAspettare.jpg#center)
This photo was taken in Parco di Monza (Italy), in February 2022.

### E uno per sognare
![E uno per sognare](4-EUnoPerSognare.jpg#center)
This photo was taken in Bonneval-sur-Arc (France), in August 2022.
