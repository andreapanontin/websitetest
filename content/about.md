---
title: "About me"
date: 2022-03-13T22:16:37+01:00
draft: false
ShowBreadCrumbs: false
hidemeta: true
showtoc: false
math: true
---

I am a PhD student at [Universität Regensburg](https://www.uni-regensburg.de/en),
working under the supervision of
[Prof. Dr. Moritz Kerz](http://www.mathematik.uni-regensburg.de/kerz/index.html)
and [Dr. Veronika Ertl](https://homepages.uni-regensburg.de/~erv10962/index.html),
supported by the project [SFB 1085 "Higher Invariants"](https://www-app.uni-regensburg.de/Fakultaeten/MAT/sfb-higher-invariants/index.php/SFB1085).

I am interested in arithmetic geometry, especially \\(p\\)-adic Hodge theory
and \\(p\\)-adic cohomologies.


### Contacts
You can find me at the
[Faculty of Mathematics](https://www.uni-regensburg.de/mathematics/faculty/home/index.html)
of [Universität Regensburg](https://www.uni-regensburg.de/en),
office M310.   
You can send me an email at ``name.surname [at] mathematik.uni-regensburg.de``.


### CV
You can find the latest version of my CV
[here](https://andreapanontin.gitlab.io/CurriculumVitae/CV_Andrea_Panontin.pdf).
