---
title: "Parole"
date: 2022-03-22T13:16:36+01:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

### 1
Il vuoto   
Una sola parola   
e più niente   

Lontano   
Un ricordo   
di condivisione   

Freddo   
Un deserto   
di solitudine   

La distanza   
Una nebbia   
di eterna attesa   

### n
Scavalca la mia cecità:   
fai risplendere la tua anima.  
Spoglia nudi i desideri   
più reconditi e segreti.   

Avviciniamo i nostri spiriti   
a danzare, sulle macerie   
delle nostre vite.   
Volteggiamo con la musica   
delle nostre voci,   
che risuonano in un   
vortice di emozioni.   

Stringiamoci sotto questa   
forte e dolce tempesta,   
che irrora la nostra anima   
con un nettare di vitalità.   
