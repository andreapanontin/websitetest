---
title: "Radici"
date: 2022-05-06T17:32:32+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Sempre nello stesso squarcio di terra,  
sei passato da un angolo a un altro.  
Le tue radici, orizzontali, non sono  
mai andate in profondità, non sono  
mai cresciute per donarti stabilità.  
Hanno sempre e solo garantito il  
minimo supporto: una folata
di vento e voli via.

Da uno sprazzo al successivo, il  
trauma non si percepisce: hai perso  
le radici... Le hai mai veramente avute?

Il processo di ricostruirle... Chiede sempre  
tempo, energie e solitudine.  
Ma senza radici non puoi vivere, non  
puoi respirare, non puoi amare.  
Ogni iterazione crescono un po'  
più forti, un po' più a fondo.

Staccarle fa più male, è traumatico.  
Ma è anche bello: quel terreno si  
ricorderà di te, finché le radici  
non nutriranno i prossimi fiori.  
Non sei più slegato dal mondo:  
hai lasciato una traccia indelebile.

Ma quali sono le tue radici?  
Sono solo nel luogo materno?  
O sono ovunque tu sia stato, nel  
cuore di chiunque ti abbia abbracciato?  

Dov'è casa tua?  
Una realizzazione liberatoria: è nelle  
braccia di chiunque voglia annaffiarmi e  
far crescere le mie radici.
