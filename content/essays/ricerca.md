---
title: "Alla ricerca di un briciolo di serenità"
date: 2022-02-26T13:04:01+01:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

Un vortice di nozioni   
aumenta d'intensità.   
Ti fa perdere l'equilibrio   
e la cognizione della realtà.   

Cadi.   
Cerchi un terreno e non lo trovi.   
Nebbia.   
Ti guardi intorno e non ti ritrovi.   
Paura.   
Sei libero da ogni vincolo:    
La luce può scaldare solo dall'interno    
