---
title: "Maschera"
date: 2016-04-30T14:33:56+01:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

Il rimpianto di   
un solo pensiero:   
ogni sorriso una maschera   
cela il velo che avvolge   
le emozioni, in un lago   
di Malinconia.
