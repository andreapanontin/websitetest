---
title: "Reflections on languages"
date: 2022-06-03T16:22:41+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

### Sprachen
A standard agreed upon by nobody.  
Rendono possibile la comunicazione  
et au même temps impossible  
den Austausch von Ideen.

### Wörter
die Zunge ist scharf,  
Wohlbefinden bringen kann,  
aber sie kann auch verletzen.  
Und niemand bringt uns bei,  
wie man sie benutzt.
