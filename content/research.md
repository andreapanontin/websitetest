---
title: "Research"
date: 2022-03-13T21:32:00+01:00
draft: false
ShowBreadCrumbs: false
hidemeta: true
showtoc: false
math: true
---

### Undergraduate work
1. Master's thesis: [Crystalline comparison theorem for
\\(p\\)-divisible groups](https://andreapanontin.gitlab.io/MastersThesis/Crystalline_comparison_theorem_for_p-divisible_groups.pdf).   
Written in [Bordeaux](https://www.math.u-bordeaux.fr/imb/?lang=fr)
under the supervision of
[Prof. Olivier Brinon](https://www.math.u-bordeaux.fr/~obrinon/).   
You can also find the pdf at [github](https://github.com/andreapanontin/MastersThesis)
or at [gitlab](https://gitlab.com/andreapanontin/MastersThesis).

2. Bachelor thesis (in Italian):
[Introduzione alla computazione quantistica](https://andreapanontin.gitlab.io/BachelorThesis/Introduzione_alla_computazione_quantistica.pdf).   
Written in [Milano](https://www.matapp.unimib.it/)
under the supervision of
[Prof. Davide L. Ferrario](https://staff.matapp.unimib.it/~ferrario/main.html).


### Some notes
1. Short notes on the [De Rham theorem](https://andreapanontin.gitlab.io/DeRham-theorem/De_Rham_and_singular_cohomology.pdf)
written in Padova, in 2020, for the course _Topology 2_, held by [Prof. Andrea D'Agnolo](http://docenti.math.unipd.it/dagnolo/).

2. Notes for the course [Rings and Modules](https://andreapanontin.gitlab.io/RingsAndModules/Rings_and_Modules.pdf)
covering the results of the course held by [Prof. Silvana Bazzoni](https://www.math.unipd.it/~bazzoni/)
in the summer semester of A.Y. 2019-2020 in Padova.
